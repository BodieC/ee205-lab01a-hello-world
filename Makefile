###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01a - Hello World - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a Hello World C program
###
### @author  Bodie Collins <Bodie@hawaii.edu>
### @date    11 Jan 2021
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = hello

all: $(TARGET)

hello: hello.c
	$(CC) $(CFLAGS) -o $(TARGET) hello.c

clean:
	rm -f $(TARGET) *.o

